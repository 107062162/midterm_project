function init_chat() {
    var user_email = '';
    var txt_original = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('sign_state');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<a class='nav-link' class='exchange'>" + user.email + "<span class='exchange' id='logout-btn'>Logout</span></a>";
            var btnLogout = document.getElementById('logout-btn');

            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut().then(function () {
                    // Sign-out successful.
                    create_alert("success", "Success Sign Out");
                }).catch(function (error) {
                    // An error happened.
                    create_alert("error", error.message);
                });
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Sign In</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {

            var newpostref = firebase.database().ref('com_list').push();
            var current_time_tmp = new Date();
            var month = current_time_tmp.getMonth() + 1;
            var timetoRecord = current_time_tmp.getFullYear() + "/" + month + "/" + current_time_tmp.getDate() + " " + current_time_tmp.getHours() + ":" + current_time_tmp.getMinutes();
            newpostref.set({
                email: user_email,
                data: post_txt.value,
                time: timetoRecord
            });
            post_txt.value = "";
        }
    });

    // The html code for post
    var str_before_username_left = "<div class='d-flex justify-content-start mb-1'><div class='msg_cotainer'><div class='media text pt-1'><p class='media-body pb-1 mb-0 small lh-125'><strong>";
    var str_before_username_right = "<div class='d-flex justify-content-end mb-1'><div class='msg_cotainer_send'><div class='media text pt-1'><p class='media-body pb-1 mb-0 small lh-125'><strong>";
    //var str_before_username = "<div><h6></h6><div class='media text-muted pt-3'><p><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div></div>\n";

    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (history) {
                var user_input = firebase.auth().currentUser;
                var historychildData = history.val();
                var user_input_email = user_input.email;
                txt_original = historychildData.data;
                for (var i = 0; i < txt_original.length; i++) {
                    txt_tmp = txt_original;
                    if (txt_tmp[i] == "<") {
                        txt_original = txt_tmp.slice(0, i) + "&lt;" + txt_tmp.slice(i + 1, txt_original.length.length);
                        i += 3;
                    }
                    else if (txt_tmp[i] == ">") {
                        txt_original = txt_tmp.slice(0, i) + "&gt;" + txt_tmp.slice(i + 1, txt_original.length.length);
                        i += 3;
                    }
                    else if (txt_tmp[i] == "&") {
                        txt_original = txt_tmp.slice(0, i) + "&amp;" + txt_tmp.slice(i + 1, txt_original.length.length);
                        i += 4;
                    }
                    else if (txt_tmp[i] == "\"") {
                        txt_original = txt_tmp.slice(0, i) + "&quot;" + txt_tmp.slice(i + 1, txt_original.length.length);
                        i += 5;
                    }
                    else if (txt_tmp[i] == "\'") {
                        txt_original = txt_tmp.slice(0, i) + "&apos;" + txt_tmp.slice(i + 1, txt_original.length.length);
                        i += 5;
                    }
                    else if (txt_tmp[i] == " ") {
                        txt_original = txt_tmp.slice(0, i) + "&nbsp;" + txt_tmp.slice(i + 1, txt_original.length.length);
                        i += 5;
                    }
                    else if (txt_tmp[i] == "\\") {
                        txt_original = txt_tmp.slice(0, i) + "\\" + txt_tmp.slice(i + 1, txt_original.length.length);
                        i += 1;
                    }
                }

                if (user_input_email == historychildData.email) {
                    total_post.push(str_before_username_right + historychildData.email + "</strong>" + "<br>" + historychildData.time + "<br>" + txt_original + str_after_content)
                }
                else {
                    total_post.push(str_before_username_left + historychildData.email + "</strong>" + "<br>" + historychildData.time + "<br>" + txt_original + str_after_content)
                }

                first_count += 1;
            })
            /// Join all post in list to html in once
            document.getElementById('post_list').innerHTML = total_post.join('');

            /// Add listener to update new post
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    //total_post[total_post.length] = str_before_username_right + childData.email + "</strong>" + "<br>" + childData.time + "<br>" + childData.data + str_after_content;
                    var user_input = firebase.auth().currentUser;
                    txt_original = childData.data;
                    for (var i = 0; i < txt_original.length; i++) {
                        txt_tmp = txt_original;
                        if (txt_tmp[i] == "<") {
                            txt_original = txt_tmp.slice(0, i) + "&lt;" + txt_tmp.slice(i + 1, txt_original.length.length);
                            i += 3;
                        }
                        else if (txt_tmp[i] == ">") {
                            txt_original = txt_tmp.slice(0, i) + "&gt;" + txt_tmp.slice(i + 1, txt_original.length.length);
                            i += 3;
                        }
                        else if (txt_tmp[i] == "&") {
                            txt_original = txt_tmp.slice(0, i) + "&amp;" + txt_tmp.slice(i + 1, txt_original.length.length);
                            i += 4;
                        }
                        else if (txt_tmp[i] == "\"") {
                            txt_original = txt_tmp.slice(0, i) + "&quot;" + txt_tmp.slice(i + 1, txt_original.length.length);
                            i += 5;
                        }
                        else if (txt_tmp[i] == "\'") {
                            txt_original = txt_tmp.slice(0, i) + "&apos;" + txt_tmp.slice(i + 1, txt_original.length.length);
                            i += 5;
                        }
                        else if (txt_tmp[i] == " ") {
                            txt_original = txt_tmp.slice(0, i) + "&nbsp;" + txt_tmp.slice(i + 1, txt_original.length.length);
                            i += 5;
                        }
                        else if (txt_tmp[i] == "\\") {
                            txt_original = txt_tmp.slice(0, i) + "\\" + txt_tmp.slice(i + 1, txt_original.length.length);
                            i += 1;
                        }
                    }
                    if (user_input.email == childData.email) {
                        total_post[total_post.length] = str_before_username_right + childData.email + "</strong>" + "<br>" + childData.time + "<br>" + txt_original + str_after_content;
                    }
                    else {
                        total_post[total_post.length] = str_before_username_left + childData.email + "</strong>" + "<br>" + childData.time + "<br>" + txt_original + str_after_content;
                    }
                    document.getElementById('post_list').innerHTML = total_post.join('');

                    if (window.Notification) {
                        //var button = document.getElementById('post_btn');
                        //text = document.getElementById('text');
                        var popNotice = function () {
                            if (Notification.permission == "granted") {
                                var notification = new Notification(childData.email, {
                                    body: childData.data,
                                    //icon: '//image.zhangxinxu.com/image/study/s/s128/mm1.jpg'
                                });
                            }
                        };
                        var user = firebase.auth().currentUser;
                        if (Notification.permission == "granted" && user_input.email != childData.email) {
                            popNotice();
                        } else if (Notification.permission != "denied" && user_input.email != childData.email) {
                            Notification.requestPermission(function (permission) {
                                popNotice();
                            });
                        }
                    } else {
                        create_alert("success", "Don't support Notification");
                    }
                }
            });
        })
        .catch(e => console.log(e.message));
}

function room_verify() {
    var flag_room = 0;
    var password = "";
    var total_room = 100000;
    var room_count = 0;
    var room_number = document.getElementById('addroom');
    var roomRef = firebase.database().ref('room_list');
    var newPostKey = firebase.database().ref('room_list');
    if (room_number.value != "") {
        roomRef.once('value')
            .then(function (snapshot) {
                total_room = snapshot.numChildren();
                snapshot.forEach(function (history) {
                    var historychildData = history.val();
                    if (room_number.value == historychildData.room_number && flag_room != 1) {
                        flag_room = 1;
                        password = historychildData.password;
                        var passsword_input = prompt("请输入密碼", "");
                        if (passsword_input == password) {
                            create_alert("success", "join_success");
                            window.location.href = "private.html" + "?key=" + history.key + "&room=" + room_number.value;
                        }
                        else {
                            room_number.value = "";
                            create_alert("error", "error");
                        }
                    }
                    room_count++;
                    if (room_count >= total_room && flag_room == 0) {
                        var passsword_input = prompt("请输入密碼", "");
                        if (passsword_input != "") {
                            var newroomref = firebase.database().ref('room_list').push();
                            newPostKey = newroomref.key;
                            newroomref.set({
                                room_number: room_number.value,
                                password: passsword_input
                            });
                            create_alert("success", "new_room");
                            window.location.href = "private.html" + "?key=" + newPostKey + "&room=" + room_number.value;
                        }
                        else {
                            room_number.value = "";
                            create_alert("error", "error");
                        }

                    }
                })
            })
            .catch(e => console.log(e.message));
    }
}

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    init_chat();
};