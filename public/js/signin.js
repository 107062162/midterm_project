function initApp() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('sign_state');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<a class='nav-link' class='exchange'>" + user.email + " " + "<span class='exchange' id='logout-btn'>Logout</span></a>";
            create_alert("success", "Success Sign In");
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            
            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut().then(function() {
                    // Sign-out successful.
                    create_alert("success", "Success Sign Out");
                  }).catch(function(error) {
                    // An error happened.
                    create_alert("error", error.message);
                  });
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Sign In</a>";
        }
    });
    
    // Login with Email/Password
    var txtEmail = document.getElementById('loginMail');
    var txtPassword = document.getElementById('loginPassword');
    var btnLogin = document.getElementById('btnSignIn');
    var btnGoogle = document.getElementById('btnGoogle');

    btnLogin.addEventListener('click', function () {
        var email = txtEmail.value;
        var password = txtPassword.value;

        firebase.auth().signInWithEmailAndPassword(email, password).then(function(){
            //alert("Sign In")
            create_alert("success", "Sign In")
            window.location.href="chatroom.html";
        }).catch(function(error) { 
            // Handle Errors here. 
            var errorCode = error.code; 
            var errorMessage = error.message;
            //alert(error.message);
            create_alert("error", errorMessage);
            txtEmail.value = '';
            txtPassword.value = '';
        });


    });

    btnGoogle.addEventListener('click', function () {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) { 
            var token = result.credential.accessToken;
            var user = result.user;
            window.location.replace("chatroom.html");
        }).catch(function(error) { 
            // Handle Errors here. 
            var errorCode = error.code; 
            var errorMessage = error.message; 
            // The email of the user's account used. 
            var email = error.email; 
            // The firebase.auth.AuthCredential type that was used. 
            var credential = error.credential;
            //alert(errorMessage)
            create_alert("error", errorMessage);
        });
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};