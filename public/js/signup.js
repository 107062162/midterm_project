function initApp() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('sign_state');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<a class='nav-link' class='exchange'>" + user.email + " " + "<span class='exchange' id='logout-btn'>Logout</span></a>";
            create_alert("success", "Success Sign In");
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            
            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut().then(function() {
                    // Sign-out successful.
                    create_alert("success", "Success Sign Out");
                  }).catch(function(error) {
                    // An error happened.
                    create_alert("error", error.message);
                  });
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Sign In</a>";
        }
    });
    // SignUp with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnSignUp = document.getElementById('btnSignUp');

    btnSignUp.addEventListener('click', function () {
        var email = txtEmail.value;
        var password = txtPassword.value;

        firebase.auth().createUserWithEmailAndPassword(email, password).then(function () {
            //alert("Success");
            create_alert("success", "Success Sign Up");
            txtEmail.value = '';
            txtPassword.value = '';
            window.location.href = "chatroom.html";
        }).catch(function (error) {
            alert(error.message);
            //create_alert("error", error.mesaage);
            txtEmail.value = '';
            txtPassword.value = '';
        });
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};