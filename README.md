# Software Studio 2020 Spring Midterm Project

## Topic
* Project Name : Midterm Project 107062162
* Key functions
    1.  Public Chatroom
    2.  Private Chatroom 

## Website Detail Description

# 作品網址：https://midterm-project-107062162.web.app

# Components Description : 
1. Sign Up
    使用email Sign Up
3. Sign In
    必須要先Sign In，否則無法在Chatroom裡面進行操作
    目前的Sign In方式有2類：Sign Up之後登入、用google登入，其餘方式待開發
    {%youtube XimTfQ5L7Wk %}
4. Public Chatroom
    任何人都能看到這個Chatroom，是使用者的交流平台
    {%youtube Xg58v-nCoLY %}
6. Private Chatroom
    必須要輸入Chatroom's name和password才能進入
    當輸入的name不存在，會自動創建一個新的Chatroom
    {%youtube 0KFmfeKYLCo %}

## Security Report (Optional)
- 使用者如果想要進入私人聊天室，必須先獲得房間名字和密碼。
- 為了保護隱私，不會記錄房間裡的使用者
